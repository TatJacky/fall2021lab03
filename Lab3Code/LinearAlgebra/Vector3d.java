//Jacky Tat 1911748
package LinearAlgebra;
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2)+Math.pow(this.z, 2));
    }

    public double dotProduct(Vector3d xVector){
        return this.x*xVector.getX()+this.y*xVector.getY()+this.z*xVector.getZ();
    }

    public Vector3d add(Vector3d oldVector){
        double x = this.x + oldVector.getX();
        double y = this.y + oldVector.getY();
        double z = this.z + oldVector.getZ();
        Vector3d newVector = new Vector3d(x,y,z);
        return newVector;
    }
}
