//Jacky Tat 1911748
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    @Test
    public void testConstructor(){
        Vector3d v = new Vector3d(2,3,4);
        assertEquals(2, v.getX());
        assertEquals(3, v.getY());
        assertEquals(4, v.getZ());
    }

    @Test
    public void testMagnitude(){
        Vector3d v = new Vector3d(2,3,4);
        assertEquals(Math.sqrt(Math.pow(2,2)+Math.pow(3,2)+Math.pow(4,2)) ,v.magnitude());

    }

    @Test
    public void testAdd(){
        Vector3d v = new Vector3d(2,3,4);
        Vector3d v2 = new Vector3d(5,6,7);
        Vector3d v3 = new Vector3d(7,9,11);
        assertEquals(v3.getX(), v.add(v2).getX());
        assertEquals(v3.getY(), v.add(v2).getY());
        assertEquals(v3.getZ(), v.add(v2).getZ());
    }
}
